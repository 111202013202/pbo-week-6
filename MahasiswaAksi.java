import mahasiswa.*;

public class MahasiswaAksi {
    public static void main(String[] args) {
        // a. Membuat objek dari class Mahasiswa
        Mahasiswa mahasiswa = new Mahasiswa("A11.2020.13202", "Anggoro Ajie Putranto", 3.94, 115, "2001-07-26");
        // b. Mencari nama program studi berdasarkan nim 
        System.out.println("Nama Program Studi : " + mahasiswa.getProgdi());
        // c. Mencari status berdasarkan ipk
        System.out.println("Status : " + mahasiswa.ipkStatus());
        // d. Mencari tahun angkatan berdasarkan nim 
        System.out.println("Tahun Angkatan : " + mahasiswa.getTahun());
        // e. Mencari tagihan berdasarkan sks
        System.out.println("Tagihan : " + mahasiswa.getTagihanSks());
        // f. Mencari berapa semester mahasiswa sudah kuliah 
        System.out.println("Semester : " + mahasiswa.getMhsSemester());
        // g. Mencari umur mahasiswa
        System.out.println("Umur : " + mahasiswa.getUmur());
    }
}
